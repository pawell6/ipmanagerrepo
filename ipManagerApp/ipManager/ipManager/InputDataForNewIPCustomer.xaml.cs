﻿using ipManager.Classes;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for InputDataForNewIPCustomer.xaml
    /// </summary>
    public partial class InputDataForNewIPCustomer : Window
    {
        public InputDataForNewIPCustomer()
        {
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            //save to DATABASE!!
            SaveToDataBase();

            //open successfully saved window           
            SuccesfullySavedWindow succesfullySavedWindow = new SuccesfullySavedWindow();
            succesfullySavedWindow.ShowDialog();

            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SaveToDataBase()
        {
            Contact contact = new Contact()
            {
                IpAddress = ipAddressTextBox.Text,
                Host = hostNameTextBox.Text,
                Location = locationTextBox.Text,
                MacAddress = MacAddressTextBox.Text,
                IpGroup = ipGroupTextBox.Text,
                VendorName = vendorNameTextBox.Text,
                DeviceName = deviceNameTextBox.Text
            };

            using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<Contact>();
                connection.Insert(contact);
            }

            Close();
        }
    }
}
