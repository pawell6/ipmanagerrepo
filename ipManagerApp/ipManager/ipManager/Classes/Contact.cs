﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ipManager.Classes
{
    public class Contact
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string Host { get; set; }
        public string Location { get; set; }
        public string MacAddress { get; set; }
        public string IpGroup { get; set; }
        public string VendorName { get; set; }
        public string DeviceName { get; set; }


        public override string ToString()
        {
            return $"{IpAddress} - {IpAddress} - {Host} - {Location} - {MacAddress} - {IpGroup} - {VendorName} - {DeviceName}";
        }
    }
}
