﻿using ipManager.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
      
        public MainWindow()
        {
            InitializeComponent(); 
        }

        private void _findFreeIPTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _findFreeIPTextBox.Text = "";
        }

        private void _showAllFromNetworkTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _showAllFromNetworkTextBox.Text = "";
        }

        private void _findGivenIPTextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _findGivenIPTextBox.Text = "";
        }

        private void FindFreeIPButton_Click(object sender, RoutedEventArgs e)
        {
            FoundedFreeIPinNetwork foundedFreeIPinNetworkWindow = new FoundedFreeIPinNetwork();
            foundedFreeIPinNetworkWindow.ShowDialog();
        }
        private void FindOutWhoHasGivenIPButton_Click(object sender, RoutedEventArgs e)
        {
            IPOwnerWindow IPOwnerWindow = new IPOwnerWindow();
            IPOwnerWindow.ShowDialog();
        }
        private void ShowAllFromGivenNetworkButton_Click(object sender, RoutedEventArgs e)
        {
            string objname = ((Button)sender).Name;
            ALLipSpreadSheetWindow allIpSpreadSheetWindow = new ALLipSpreadSheetWindow(objname);
            allIpSpreadSheetWindow.ShowDialog();
        }

        private void ShowAllFromGivenSiteButton_Click(object sender, RoutedEventArgs e)
        {
            string objname = ((Button)sender).Name;
            ALLipSpreadSheetWindow allIpSpreadSheetWindow = new ALLipSpreadSheetWindow(objname);
            allIpSpreadSheetWindow.ShowDialog();
        }

  
        //
    }
}
