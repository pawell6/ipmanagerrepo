﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for FoundedFreeIPinNetwork.xaml
    /// </summary>
    public partial class FoundedFreeIPinNetwork : Window
    {
        int amountOfFoundedIP = 1;
        public FoundedFreeIPinNetwork()
        {
            
            InitializeComponent();
      
            amountOfFounded.Content = $"{amountOfFoundedIP} empty IP founded";

            if (amountOfFoundedIP == 0)
                YesButton.Visibility = Visibility.Hidden;

            YesButton.Click += YesButton_Click;
            NoButton.Click += NoButton_Click;
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            //open successfully saved window           
            InputDataForNewIPCustomer inputDataForNewIPCustomer = new InputDataForNewIPCustomer();
            inputDataForNewIPCustomer.ShowDialog();

            this.Close();
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
