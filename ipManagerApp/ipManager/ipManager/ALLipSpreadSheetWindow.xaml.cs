﻿using ipManager.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for ALLipSpreadSheetWindow.xaml
    /// </summary>
    public partial class ALLipSpreadSheetWindow : Window
    {
        public static ALLipSpreadSheetWindow Instance { get; private set; }

        List<Contact> contacts;

        public ALLipSpreadSheetWindow(string name)
        {
            Instance = this;
            InitializeComponent();

            contacts = new List<Contact>();

            ReadDatabase();
            Console.WriteLine("Nazywam sie" + name);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NewContactWindow newContactWindow = new NewContactWindow();
            newContactWindow.ShowDialog();

            ReadDatabase();
        }
        public void ReadDatabase()
        {
            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.databasePath))
            {
                conn.CreateTable<Contact>();
                contacts = (conn.Table<Contact>().ToList()).OrderBy(c => c.DeviceName).ToList();
            }

            if (contacts != null)
            {
                contactsListView.ItemsSource = contacts;
            }
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox searchTextBox = sender as TextBox;

            var filteredList = contacts.Where(c => c.DeviceName.ToLower().Contains(searchTextBox.Text.ToLower())).ToList();

            var filteredList2 = (from c2 in contacts
                                 where c2.DeviceName.ToLower().Contains(searchTextBox.Text.ToLower())
                                 orderby c2.VendorName
                                 select c2).ToList();

            contactsListView.ItemsSource = filteredList;
        }



        private void contactsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Contact selectedContact = (Contact)contactsListView.SelectedItem;

            if (selectedContact != null)
            {
                ContactDetailsWindow contactDetailsWindow = new ContactDetailsWindow(selectedContact);
                contactDetailsWindow.ShowDialog();
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.databasePath))
            {
                conn.CreateTable<Contact>();
                contacts = (conn.Table<Contact>().ToList()).OrderBy(c => c.DeviceName).ToList();
            }

            if (contacts != null)
            {
                contactsListView.ItemsSource = contacts;
            }
          
        }
    }
}
