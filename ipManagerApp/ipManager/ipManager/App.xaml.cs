﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static string databaseName = "Contacts2.db";
        //static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //static string folderPath = @"C:\Users\pawel.labuz\Downloads";
        static string folderPath = @"\\10.32.182.41\File_Server\17. IT\PawelBazaDanych";
        public static string databasePath = System.IO.Path.Combine(folderPath, databaseName);
    }
}
