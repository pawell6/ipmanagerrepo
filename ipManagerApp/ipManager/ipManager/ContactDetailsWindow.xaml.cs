﻿using ipManager.Classes;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ipManager
{
    /// <summary>
    /// Interaction logic for ContactDetailsWindow.xaml
    /// </summary>
    public partial class ContactDetailsWindow : Window
    {
        Contact contact;
        public ContactDetailsWindow(Contact contact)
        {
            InitializeComponent();

            this.contact = contact;
            //nameTextBox.Text = contact.Name;
            //phoneNumberTextBox.Text = contact.Phone;
            //emailTextBox.Text = contact.Email;

            ipAddressTextBox.Text = contact.IpAddress;
            hostNameTextBox.Text = contact.Host;
            locationTextBox.Text = contact.Location;
            MacAddressTextBox.Text = contact.MacAddress;
            ipGroupTextBox.Text = contact.IpGroup;
            vendorNameTextBox.Text = contact.VendorName;
            deviceNameTextBox.Text = contact.DeviceName;
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            //contact.Name = nameTextBox.Text;
            //contact.Phone = phoneNumberTextBox.Text;
            //contact.Email = emailTextBox.Text;

            contact.IpAddress = ipAddressTextBox.Text;
            contact.Host = hostNameTextBox.Text;
            contact.Location = locationTextBox.Text;
            contact.MacAddress = MacAddressTextBox.Text;
            contact.IpGroup = ipGroupTextBox.Text;
            contact.VendorName = vendorNameTextBox.Text;
            contact.DeviceName = deviceNameTextBox.Text;

            using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<Contact>();
                connection.Update(contact);
            }

            
            Close();
            ALLipSpreadSheetWindow.Instance.ReadDatabase();
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<Contact>();
                connection.Delete(contact);
            }


           
            Close();

          
            ALLipSpreadSheetWindow.Instance.ReadDatabase();
        }
    }
}
